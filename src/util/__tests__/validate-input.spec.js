import { validateInput } from '../validate-input.js';
import {
  MOVE,
  LEFT,
  RIGHT,
  REPORT,
  X_UP,
  Y_UP,
  NORTH,
  EAST,
  SOUTH,
  WEST
} from '../../data/consts.js';

describe('Tests for the validate input function.', () => {
  // Stub out console log for cleaner terminal.
  console.log = jest.fn();

  describe('Tests for the basic commands and false commands.', () => {
    // Tests for basic commands that should return true.
    const truthyTests = [
      [true, MOVE],
      [true, LEFT],
      [true, REPORT],
      [true, RIGHT]
    ];
    // Tests that should fail.
    const falsyTests = [
      [false, 'TEST'],
      [false, 'move'],
      [false, `${MOVE}TEST`],
      [false, `TEST${MOVE}`],
      [false, `${MOVE} TEST`],
      [false, `${MOVE}${RIGHT}`]
    ];
    const tests = [...truthyTests, ...falsyTests];

    it.each(tests)(
      `Should return %p, when input is: %p and hasPosition is true`,
      (expected, input) => {
        const res = validateInput(input, true);
        expect(res).toEqual(expected);
      }
    );
  });

  describe('Tests for the place command.', () => {
    // Tests for each direction and within the all the corners of the board.
    const truthyTests = [
      [true, `PLACE 0,0,${NORTH}`],
      [true, `PLACE ${X_UP},0,${EAST}`],
      [true, `PLACE 0,${Y_UP},${SOUTH}`],
      [true, `PLACE ${X_UP},${Y_UP},${WEST}`]
    ];
    // Tests that should fail.
    const falsyTests = [
      [false, `PLACE 0,0,0`],
      [false, `PLACE 0,0,${NORTH},0`],
      [false, `PLACE ${NORTH},0,0`],
      [false, `PLACE 0,${NORTH},0`],
      [false, `PLACE -1,0,${NORTH}`],
      [false, `PLACE 0,-1,${NORTH}`],
      [false, `PLACE ${X_UP + 1},0,${NORTH}`],
      [false, `PLACE 0,${Y_UP + 1},${NORTH}`]
    ];
    const tests = [...truthyTests, ...falsyTests];

    it.each(tests)(
      `Should return %p, when input is: %p and hasPosition is true`,
      (expected, input) => {
        const res = validateInput(input, true);
        expect(res).toEqual(expected);
      }
    );
  });

  describe('Tests for the first command functionality', () => {
    const tests = [
      [true, `PLACE 0,0,${NORTH}`],
      [false, `PLACE 0,0,${MOVE}`]
    ];

    it.each(tests)(
      `Should return %p, when input is: %p and hasPosition is false`,
      (expected, input) => {
        const res = validateInput(input, false);
        expect(res).toEqual(expected);
      }
    );
  });
});
