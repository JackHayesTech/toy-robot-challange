import {
  NORTH,
  SOUTH,
  EAST,
  WEST,
  BOUNDS_ERROR,
  Y_UP,
  X_UP
} from '../data/consts.js';

// List of valid directions.
const directions = [NORTH, EAST, SOUTH, WEST];

/**
 * The Robot class
 */
export class Robot {
  constructor() {
    this.position = null;
  }

  /**
   * Checks if the robot has a position.
   * @returns {boolean} If the robot has been placed or not.
   */
  isPlaced() {
    if (this.position !== null) return true;
    return false;
  }

  /**
   * Sets the robots position on the board.
   * @param {string} x - X coordinate on board.
   * @param {string} y - Y coordinate on board.
   * @param {NORTH|EAST|SOUTH|WEST} f - The Direction the robot faces.
   */
  place(x, y, f) {
    this.position = { x: parseInt(x, 10), y: parseInt(y, 10), f };
  }

  /**
   * Outputs the robots position and direction to the console.
   */
  report() {
    const { x, y, f } = this.position;
    console.log(`Position ${x}, ${y}\nFacing: ${f}`);
  }

  /**
   * Rotates the robot in a certain direction.
   * @param {1|-1} direction - 1 turns the robot 90 degrees right, -1 turns the robot 90 degrees left.
   */
  rotate(direction) {
    let { f } = this.position;

    // Gets the current direction index of the robot and applies the new direction to it.
    // This approach allows the for the simple addition of extra directions such as NW.
    let d = directions.indexOf(f) + direction;

    // Loops the robots direction to the top of the array.
    if (d < 0) d = directions.length - 1;
    // Loops the robots direction to the bottom of the array.
    if (d > directions.length - 1) d = 0;

    // Updates the robots position.
    this.position.f = directions[d];
  }

  /**
   * Sets the robots position on the table.
   * @param {x|y} plane - The X or Y plane to increment by.
   * @param {1|-1} increment - Which direction to move on the coord plane
   * @param {*} upper - The upper bounds of the coord plane.
   * @returns {undefined}
   */
  updatePos(plane, increment, upper) {
    const val = this.position[plane] + increment;

    // The robot has attempted to move out of the table coordinates.
    if (val < 0 || val > upper) {
      console.log(BOUNDS_ERROR);
      return;
    }

    this.position[plane] = val;
  }

  /**
   * Based on the robots direction move the robot 1 square forward.
   */
  move() {
    const { f } = this.position;

    switch (f) {
      case NORTH:
        this.updatePos('y', 1, Y_UP);
        break;
      case EAST:
        this.updatePos('x', 1, X_UP);
        break;
      case SOUTH:
        this.updatePos('y', -1, Y_UP);
        break;
      case WEST:
        this.updatePos('x', -1, X_UP);
        break;
    }
  }
}
