import {
  PLACE,
  MOVE,
  LEFT,
  RIGHT,
  REPORT,
  FIRST_COMMAND,
  INVALID_COMMAND
} from '../data/consts.js';

// Regex for the valid commands that can be called.
const validCommands = `^(\\b${PLACE}\\b|\\b${MOVE}\\b|\\b${LEFT}\\b|\\b${RIGHT}\\b|\\b${REPORT}\\b)*$`;

/**
 * Validates the user provided input against the possible valid commands.
 * @param {string} input - The user input.
 * @param {boolean} hasPosition - The position of the robot.
 * @returns {boolean} - If the command was valid or not.
 */
export const validateInput = (input, hasPosition) => {
  // Ensures that if the robot isn't on the board yet the first command must be PLACE.
  if (!hasPosition && !input.match(PLACE)) {
    console.log(FIRST_COMMAND);
    return false;
  }

  // Validates against the regex to ensure a valid command has been used.
  if (input.match(validCommands)) return true;

  // An invalid command was used.
  console.log(INVALID_COMMAND);
  return false;
};
